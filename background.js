
var s = document.createElement('script');
s.src = 'https://code.jquery.com/jquery-1.11.0.min.js';
s.type = 'text/javascript';
s.onload = function() { 
	doWork()
}
document.getElementsByTagName('head')[0].appendChild(s);



function join(arr, splitter) {
	res = orderList.reduce(function(a,b){ return a+splitter+b.tracking[0].mailNo }, "")
	return res.substring(splitter.length, res.length)
}

function makeLink(nextElement, color, text, href) {
	trackAllLink = nextElement.prepend('<center><p>&nbsp;</p><p><a href="'+href+'" target="_blank">'+text+'</a></p></center>')
	$(trackAllLink[0].firstElementChild.lastElementChild).css('background-color',color)
}


totalOrders = 0
orderList = []

function vmz_showOrderInfo(o, trackingArr) {
	desc = trackingArr[0].keyDesc
	numb = trackingArr[0].mailNo

	if (!desc || !numb) {
		return
	}

	trackingURL = 'https://www.pochta.ru/TRACKING#'+numb

	trackingCell = $(o).closest('td').prev()
	trackingCell.append('<p>&nbsp;</p><p>'+desc+'</p><p><a href="'+trackingURL+'" target="_blank">'+numb+'</a></p>')

	if (desc.includes('Arrived')) {
		trackingCell.css('background-color','Yellow')
	}
	else if (desc.includes('Depart')) {
		trackingCell.css('background-color','SkyBlue')
	}
	else if (desc.includes('Seller')) {
		trackingCell.css('background-color','Gainsboro') 
	}
	else if (desc.includes('In')) {
		trackingCell.css('background-color','PeachPuff') 
	}
	else if (desc.includes('shipping')) {
		trackingCell.css('background-color','GreenYellow') // can be picked up
	}
}

function vmz_getOrderInfo(o, orderid) {
	$.ajax({
		url: "https://ilogisticsaddress.aliexpress.com/ajax_logistics_track.htm?orderId="+orderid+"&callback=callbackMock",
		type: 'GET',
		dataType: 'text',
		headers: {
			//'Access-Control-Request-Headers': 'x-requested-with'
		},
		 //		xhrFields: {
		//	 withCredentials: true
		// },
			// crossDomain: true,
		success: function (text, textStatus, xhr) {

			try {

				totalOrders -= 1

				data = text.replace('callbackMock(', '').slice(0, -1)
				data = JSON.parse(data)

				var l = {}
				l.url = data.cainiaoUrl
				l.tracking = data.tracking
				l.title = $('.baobei-name', $(o).closest('tr')).toArray()[0].title

				vmz_showOrderInfo(o, data.tracking)

				// ignore finished ones
				if (l.tracking[0].keyDesc != null) {
					orderList.push(l)
				}

				console.log(totalOrders+' left');

				if (totalOrders == 0) {
					json = JSON.stringify(orderList)

					allNums = join(orderList, ',')

					setTimeout(function(allNums) {
						next = $('#buyer-ordertable').parent()
						color = 'GreenYellow'

						makeLink(next, color, 'Track at cainiao.com', 'https://global.cainiao.com/detail.htm?mailNoList='+allNums)
						makeLink(next, color, 'Track at Почта России', 'https://www.pochta.ru/tracking#'+allNums)
					}, 2000, allNums)


					console.log(orderList)
					console.log(json)

					navigator.clipboard.writeText(json).then(function() {
						console.log('Async: Copying to clipboard was successful!');
					}, function(err) {
						console.error('Async: Could not copy json: ', err);
					});
				}
			} catch(err) {

			}
		},
		error: function (xhr, textStatus, errorThrown) {
			totalOrders -= 1
			console.log(xhr, textStatus, errorThrown);
		}
	});
}

function doWork() {

	console.log('Evaluating ' + $('.order-action').toArray().length + ' orders shipment statuses...')

	$('.order-action').toArray().forEach(function(o) { 
		var orderid = $(o).attr('orderid')
		if (orderid == null) {
			return
		}

		totalOrders += 1

		vmz_getOrderInfo(o, orderid)
	})

}