# aliexpress.com nice tracking info
## An extension for Google Chrome

> If you ever wondered why the hell you need to hover that button and wait for the tracking info to load, this is for you.

### Features

* Shows shipment status and tracking number for each order in order list
* Each order's cell is colored depending on its status

| Color         | Description                    |
| ------------- | ------------------------------ |
| ... | some statuses skipped |
| Yellow | Arrived at destination country |
| GreenYellow | Arrived at post office |


### Installation

Chrome -> Extensions -> Add unpacked extension

### Screenshot

![Screenshot](images/screenshot.png)

